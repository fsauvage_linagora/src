# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2022 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# person_in_charge: simon.raude at edf.fr

DEBUT(CODE=_F(NIV_PUB_WEB="INTERNET"), DEBUG=_F(SDVERI="NON"))
# SDVERI='NON' car la verification est trop couteuse en CPU

UN = DEFI_CONSTANTE(VALE=1.0)

ZERO = DEFI_CONSTANTE(VALE=0.0)

VISCOLIQ = DEFI_CONSTANTE(VALE=1.0e-3)

VISCOGAZ = DEFI_CONSTANTE(VALE=1.8e-5)

DVISCOL = DEFI_CONSTANTE(VALE=0.0)

DVISCOG = DEFI_CONSTANTE(VALE=0.0)

KINT = DEFI_CONSTANTE(VALE=1.0e-18)

LAMBLIQ = DEFI_CONSTANTE(VALE=0.6)
LAMBGAZ = DEFI_CONSTANTE(VALE=0.03)


####  FONCTION SATURATION ET SA DERIVEE : UNITE DE PRESSION : PA
LI2 = DEFI_LIST_REEL(DEBUT=0.0, INTERVALLE=_F(JUSQU_A=1.6e8, PAS=1.0e6))

SL = FORMULE(VALE=" 0.99*(1.-PCAP*6.E-9)", NOM_PARA="PCAP")
SATU = CALC_FONC_INTERP(
    FONCTION=SL,
    LIST_PARA=LI2,
    NOM_PARA="PCAP",
    PROL_DROITE="CONSTANT",
    PROL_GAUCHE="CONSTANT",
    INFO=2,
)

DSL = FORMULE(VALE=" -6.E-9*0.99", NOM_PARA="PCAP")

DSATU = CALC_FONC_INTERP(
    FONCTION=DSL,
    LIST_PARA=LI2,
    NOM_PARA="PCAP",
    PROL_DROITE="CONSTANT",
    PROL_GAUCHE="CONSTANT",
    INFO=2,
)

# ***********************************************************************
#    MAILLAGE + MODELE
# ***********************************************************************

MAILLAGE = LIRE_MAILLAGE(FORMAT="MED")

MODELE = AFFE_MODELE(
    MAILLAGE=MAILLAGE, AFFE=_F(TOUT="OUI", PHENOMENE="MECANIQUE", MODELISATION="3D_HHM")
)
MAILLAGE = DEFI_GROUP(
    reuse=MAILLAGE,
    MAILLAGE=MAILLAGE,
    CREA_GROUP_MA=_F(NOM="ROCHE", TOUT="OUI"),
    CREA_GROUP_NO=_F(GROUP_MA="ROCHE"),
)

MAILLAGE = MODI_MAILLAGE(
    reuse=MAILLAGE, MAILLAGE=MAILLAGE, ORIE_PEAU=_F(GROUP_MA_PEAU=("HAUT", "DEVANT", "DROITE"))
)


THMALP1 = DEFI_CONSTANTE(VALE=0.000100)

SOL = DEFI_MATERIAU(
    ELAS=_F(E=7.2e6, NU=0.3, RHO=2500.0, ALPHA=1.0e-5),
    BARCELONE=_F(
        MU=2.76e6,
        PORO=0.14,
        LAMBDA=0.2,
        KAPA=0.02,
        M=1.0,
        PRES_CRIT=2.0e5,
        PA=1.0e5,
        R=0.75,
        BETA=12.5e-6,
        KC=0.6,
        PC0_INIT=3.0e5,
        KAPAS=0.008,
        LAMBDAS=0.08,
    ),
    COMP_THM="LIQU_GAZ",
    THM_LIQU=_F(
        RHO=1000.0, UN_SUR_K=0.5e-9, ALPHA=THMALP1, CP=4180.0, VISC=VISCOLIQ, D_VISC_TEMP=DVISCOL
    ),
    THM_GAZ=_F(MASS_MOL=28.96e-3, CP=1000.0, VISC=VISCOGAZ, D_VISC_TEMP=DVISCOG),
    THM_VAPE_GAZ=_F(MASS_MOL=18.0e-3, CP=1870.0, VISC=VISCOGAZ, D_VISC_TEMP=ZERO),
    THM_DIFFU=_F(
        R_GAZ=8.315,
        RHO=2400.0,
        CP=800.0,
        BIOT_COEF=1.0,
        SATU_PRES=SATU,
        D_SATU_PRES=DSATU,
        PESA_X=0.0,
        PESA_Y=0.0,
        PESA_Z=0.0,
        PERM_IN=KINT,
        PERM_LIQU=UN,
        D_PERM_LIQU_SATU=ZERO,
        PERM_GAZ=UN,
        D_PERM_SATU_GAZ=ZERO,
        D_PERM_PRES_GAZ=ZERO,
    ),
    THM_INIT=_F(TEMP=293.0, PRE1=0.0e5, PRE2=1.0e5, PORO=0.14, PRES_VAPE=2269.8, DEGR_SATU=0.99),
)

CHMAT = AFFE_MATERIAU(MAILLAGE=MAILLAGE, AFFE=_F(TOUT="OUI", MATER=SOL))

PR_LATE = AFFE_CHAR_MECA(MODELE=MODELE, PRES_REP=_F(GROUP_MA=("DEVANT", "DROITE"), PRES=1.0))

PR_V = AFFE_CHAR_MECA(MODELE=MODELE, PRES_REP=_F(GROUP_MA="HAUT", PRES=1.0))

DEP_SYM = AFFE_CHAR_MECA(
    MODELE=MODELE,
    FACE_IMPO=(
        _F(GROUP_MA="BAS", DZ=0.0),
        _F(GROUP_MA="DERRIERE", DX=0.0),
        _F(GROUP_MA="GAUCHE", DY=0.0),
    ),
)

P0 = AFFE_CHAR_MECA(MODELE=MODELE, DDL_IMPO=_F(GROUP_NO="ROCHE", PRE1=1.0, PRE2=0.0))


TEMPS1 = DEFI_LIST_REEL(
    DEBUT=0.0,
    INTERVALLE=(_F(JUSQU_A=1.0, NOMBRE=10), _F(JUSQU_A=3.0, NOMBRE=5), _F(JUSQU_A=6.0, NOMBRE=5)),
)
TEMPS2 = DEFI_LIST_REEL(
    DEBUT=6.0,
    INTERVALLE=(
        _F(JUSQU_A=10.0, NOMBRE=20),
        _F(JUSQU_A=15.0, NOMBRE=80),
        _F(JUSQU_A=20.0, NOMBRE=100),
    ),
)
TEMPS3 = DEFI_LIST_REEL(
    DEBUT=20.0,
    INTERVALLE=(_F(JUSQU_A=22.0, NOMBRE=5), _F(JUSQU_A=29.0, NOMBRE=5), _F(JUSQU_A=34.0, NOMBRE=5)),
)
TEMPS4 = DEFI_LIST_REEL(
    DEBUT=34.0,
    INTERVALLE=(_F(JUSQU_A=36.0, NOMBRE=5), _F(JUSQU_A=41.0, NOMBRE=5), _F(JUSQU_A=46.0, NOMBRE=5)),
)
TEMPS5 = DEFI_LIST_REEL(
    DEBUT=46.0,
    INTERVALLE=(
        _F(JUSQU_A=48.0, NOMBRE=5),
        _F(JUSQU_A=55.0, NOMBRE=5),
        _F(JUSQU_A=58.5, NOMBRE=9),
        _F(JUSQU_A=60, NOMBRE=50),
    ),
)

# AFFECTATION DE L ETAT INITIAL
#
DEPINIT = CREA_CHAMP(
    MAILLAGE=MAILLAGE,
    OPERATION="AFFE",
    TYPE_CHAM="NOEU_DEPL_R",
    AFFE=(
        _F(TOUT="OUI", NOM_CMP=("DX", "DY", "DZ", "PRE1", "PRE2"), VALE=(0.0, 0.0, 0.0, 0.0, 0.0)),
        _F(GROUP_NO="ROCHE", NOM_CMP="PRE1", VALE=2.0e5),
        _F(GROUP_NO="ROCHE", NOM_CMP="PRE2", VALE=0.0),
    ),
)


SIGINIT = CREA_CHAMP(
    MAILLAGE=MAILLAGE,
    OPERATION="AFFE",
    TYPE_CHAM="CART_SIEF_R",
    AFFE=_F(
        TOUT="OUI",
        NOM_CMP=(
            "SIXX",
            "SIYY",
            "SIZZ",
            "SIXY",
            "SIXZ",
            "SIYZ",
            "SIPXX",
            "SIPYY",
            "SIPZZ",
            "SIPXY",
            "SIPXZ",
            "SIPYZ",
            "M11",
            "FH11X",
            "FH11Y",
            "FH11Z",
            "ENT11",
            "M12",
            "FH12X",
            "FH12Y",
            "FH12Z",
            "ENT12",
            "M21",
            "FH21X",
            "FH21Y",
            "FH21Z",
            "ENT21",
            "QPRIM",
            "FHTX",
            "FHTY",
            "FHTZ",
        ),
        VALE=(
            -7e4,
            -7e4,
            -7e4,
            0.0,
            0.0,
            0.0,
            -8e4,
            -8e4,
            -8e4,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            -200.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
        ),
    ),
)
# CALCUL HYDROSTATIQUE

EVO_DES = DEFI_FONCTION(
    NOM_PARA="INST",
    PROL_DROITE="CONSTANT",
    PROL_GAUCHE="CONSTANT",
    VALE=(0.0, 2.0e5, 2.0, 2.0e5, 4.0, 2.0e5, 5.0, 2.0e5, 6.0, 2.0e5),
)
EVOL_HYD = DEFI_FONCTION(
    NOM_PARA="INST", PROL_DROITE="CONSTANT", VALE=(0.0, 1.5e5, 1.0, 2.0e5, 3.0, 3.0e5, 6.0, 8.0e5)
)
U1 = STAT_NON_LINE(
    ETAT_INIT=_F(SIGM=SIGINIT, DEPL=DEPINIT),
    MODELE=MODELE,
    CHAM_MATER=CHMAT,
    EXCIT=(
        _F(CHARGE=PR_LATE, FONC_MULT=EVOL_HYD),
        _F(CHARGE=PR_V, FONC_MULT=EVOL_HYD),
        _F(CHARGE=DEP_SYM),
        _F(CHARGE=P0, FONC_MULT=EVO_DES),
    ),
    COMPORTEMENT=_F(
        RELATION="KIT_HHM", RELATION_KIT=("BARCELONE", "LIQU_GAZ", "HYDR_UTIL"), ITER_INTE_MAXI=300
    ),
    NEWTON=_F(MATRICE="TANGENTE", REAC_ITER=1),
    INCREMENT=_F(INST_INIT=0.0, LIST_INST=TEMPS1),
    CONVERGENCE=_F(
        ITER_GLOB_MAXI=50,
        RESI_REFE_RELA=1.0e-3,
        SIGM_REFE=1.0e6,
        FLUX_HYD1_REFE=1.0,
        FLUX_HYD2_REFE=1.0,
        #               RESI_GLOB_RELA = 1.E-6,
    ),
)


#  TRIAXIAL
# ***********************************************************************


EVOL_DEV = DEFI_FONCTION(
    NOM_PARA="INST",
    PROL_DROITE="CONSTANT",
    VALE=(6.0, 8.0e5, 10.0, 9.0e5, 15.0, 10.0e5, 20.0, 11.0e5),
)
EVO_DES1 = DEFI_FONCTION(
    NOM_PARA="INST",
    PROL_DROITE="CONSTANT",
    PROL_GAUCHE="CONSTANT",
    VALE=(6.0, 2.0e5, 10.0, 2.0e5, 15.0, 2.0e5, 20.0, 2.0e5),
)


U1 = STAT_NON_LINE(
    reuse=U1,
    MODELE=MODELE,
    CHAM_MATER=CHMAT,
    EXCIT=(
        _F(CHARGE=DEP_SYM),
        _F(CHARGE=PR_LATE, FONC_MULT=EVOL_HYD),
        _F(CHARGE=PR_V, FONC_MULT=EVOL_DEV),
        _F(CHARGE=P0, FONC_MULT=EVO_DES1),
    ),
    COMPORTEMENT=_F(
        RELATION="KIT_HHM", RELATION_KIT=("BARCELONE", "LIQU_GAZ", "HYDR_UTIL"), ITER_INTE_MAXI=300
    ),
    ETAT_INIT=_F(EVOL_NOLI=U1),
    INCREMENT=_F(LIST_INST=TEMPS2, INST_INIT=6.0, INST_FIN=20.0),
    NEWTON=_F(MATRICE="TANGENTE", REAC_ITER=1),
    CONVERGENCE=_F(
        RESI_REFE_RELA=1.0e-3,
        SIGM_REFE=1.0e6,
        FLUX_HYD1_REFE=1.0,
        FLUX_HYD2_REFE=1.0,
        #               RESI_GLOB_RELA = 1.E-6,
        ITER_GLOB_MAXI=50,
    ),
)

# DECHARGE TRIAXIALE

EVOT_DEC = DEFI_FONCTION(
    NOM_PARA="INST",
    PROL_DROITE="CONSTANT",
    VALE=(20.0, 11.0e5, 22.0, 10.0e5, 29.0, 8.0e5, 34.0, 6.0e5),
)
EVO_DES2 = DEFI_FONCTION(
    NOM_PARA="INST",
    PROL_DROITE="CONSTANT",
    PROL_GAUCHE="CONSTANT",
    VALE=(20.0, 2.0e5, 22.0, 2.0e5, 29.0, 2.0e5, 34.0, 2.0e5),
)

U1 = STAT_NON_LINE(
    reuse=U1,
    MODELE=MODELE,
    CHAM_MATER=CHMAT,
    EXCIT=(
        _F(CHARGE=DEP_SYM),
        _F(CHARGE=PR_LATE, FONC_MULT=EVOL_HYD),
        _F(CHARGE=PR_V, FONC_MULT=EVOT_DEC),
        _F(CHARGE=P0, FONC_MULT=EVO_DES2),
    ),
    COMPORTEMENT=_F(
        RELATION="KIT_HHM", RELATION_KIT=("BARCELONE", "LIQU_GAZ", "HYDR_UTIL"), ITER_INTE_MAXI=300
    ),
    ETAT_INIT=_F(EVOL_NOLI=U1),
    INCREMENT=_F(LIST_INST=TEMPS3, INST_INIT=20.0, INST_FIN=34.0),
    NEWTON=_F(MATRICE="TANGENTE", REAC_ITER=1),
    CONVERGENCE=_F(
        RESI_REFE_RELA=1.0e-3,
        SIGM_REFE=1.0e6,
        FLUX_HYD1_REFE=1.0,
        FLUX_HYD2_REFE=1.0,
        #               RESI_GLOB_RELA = 1.E-6,
        ITER_GLOB_MAXI=50,
    ),
)

# DECHARGE HYDROSTATIQUE

EVOH_DEC = DEFI_FONCTION(
    NOM_PARA="INST",
    PROL_DROITE="CONSTANT",
    VALE=(34.0, 6.0e5, 36.0, 5.0e5, 41.0, 4.0e5, 46.0, 1.5e5),
)

EVO_DES3 = DEFI_FONCTION(
    NOM_PARA="INST",
    PROL_DROITE="CONSTANT",
    PROL_GAUCHE="CONSTANT",
    VALE=(34.0, 2.0e5, 36.0, 2.0e5, 41.0, 2.0e5, 46.0, 2.0e5),
)
U1 = STAT_NON_LINE(
    reuse=U1,
    MODELE=MODELE,
    CHAM_MATER=CHMAT,
    EXCIT=(
        _F(CHARGE=DEP_SYM),
        _F(CHARGE=PR_LATE, FONC_MULT=EVOH_DEC),
        _F(CHARGE=PR_V, FONC_MULT=EVOH_DEC),
        _F(CHARGE=P0, FONC_MULT=EVO_DES3),
    ),
    COMPORTEMENT=_F(
        RELATION="KIT_HHM", RELATION_KIT=("BARCELONE", "LIQU_GAZ", "HYDR_UTIL"), ITER_INTE_MAXI=300
    ),
    ETAT_INIT=_F(EVOL_NOLI=U1),
    INCREMENT=_F(LIST_INST=TEMPS4, INST_INIT=34.0, INST_FIN=46.0),
    NEWTON=_F(MATRICE="TANGENTE", REAC_ITER=1),
    CONVERGENCE=_F(
        RESI_REFE_RELA=1.0e-3,
        SIGM_REFE=1.0e6,
        FLUX_HYD1_REFE=1.0,
        FLUX_HYD2_REFE=1.0,
        #               RESI_GLOB_RELA = 1.E-6,
        ITER_GLOB_MAXI=50,
    ),
)

# TRIAXIAL EN DILATANCE

DEP_V = AFFE_CHAR_MECA(MODELE=MODELE, FACE_IMPO=_F(GROUP_MA="HAUT", DZ=1.0))


EVOL_DEP = DEFI_FONCTION(
    NOM_PARA="INST",
    PROL_DROITE="CONSTANT",
    VALE=(46.0, -1.75288e-02, 48.0, -3.75288e-02, 55.0, -5.75288e-02, 60.0, -6.75288e-02),
)
EVO_DES4 = DEFI_FONCTION(
    NOM_PARA="INST",
    PROL_DROITE="CONSTANT",
    PROL_GAUCHE="CONSTANT",
    VALE=(46.0, 2.0e5, 48.0, 2.0e5, 55.0, 2.0e5, 60.0, 2.0e5),
)

U1 = STAT_NON_LINE(
    reuse=U1,
    MODELE=MODELE,
    CHAM_MATER=CHMAT,
    EXCIT=(
        _F(CHARGE=DEP_SYM),
        _F(CHARGE=PR_LATE, FONC_MULT=EVOH_DEC),
        _F(CHARGE=DEP_V, FONC_MULT=EVOL_DEP),
        _F(CHARGE=P0, FONC_MULT=EVO_DES4),
    ),
    COMPORTEMENT=_F(
        RELATION="KIT_HHM", RELATION_KIT=("BARCELONE", "LIQU_GAZ", "HYDR_UTIL"), ITER_INTE_MAXI=300
    ),
    ETAT_INIT=_F(EVOL_NOLI=U1),
    INCREMENT=_F(LIST_INST=TEMPS5, INST_INIT=46.0, INST_FIN=60.0),
    NEWTON=_F(MATRICE="TANGENTE", REAC_ITER=1),
    CONVERGENCE=_F(
        RESI_REFE_RELA=1.0e-3,
        SIGM_REFE=1.0e6,
        FLUX_HYD1_REFE=1.0,
        FLUX_HYD2_REFE=1.0,
        #               RESI_GLOB_RELA = 1.E-6,
        ITER_GLOB_MAXI=50,
    ),
)

U1 = CALC_CHAMP(reuse=U1, CONTRAINTE=("SIGM_ELNO"), VARI_INTERNE=("VARI_ELNO"), RESULTAT=U1)


U1 = CALC_CHAMP(reuse=U1, RESULTAT=U1, CONTRAINTE="SIGM_NOEU", VARI_INTERNE="VARI_NOEU")


#
# -------------------------
#    TEST_RESU
# -------------------------

TEST_RESU(
    RESU=(
        _F(
            INST=6.0,
            RESULTAT=U1,
            NOM_CHAM="DEPL",
            GROUP_NO="NO8",
            NOM_CMP="DZ",
            VALE_CALC=-0.018404855001472,
        ),
        _F(
            INST=20.0,
            RESULTAT=U1,
            NOM_CHAM="DEPL",
            GROUP_NO="NO8",
            NOM_CMP="DZ",
            VALE_CALC=-0.069538215915458,
        ),
        _F(
            INST=34.0,
            RESULTAT=U1,
            NOM_CHAM="DEPL",
            GROUP_NO="NO8",
            NOM_CMP="DZ",
            VALE_CALC=-7.8130199799142e-03,
        ),
        _F(
            INST=46.0,
            RESULTAT=U1,
            NOM_CHAM="DEPL",
            GROUP_NO="NO8",
            NOM_CMP="DZ",
            VALE_CALC=-0.017410825757889,
        ),
        _F(
            INST=60.0,
            RESULTAT=U1,
            NOM_CHAM="DEPL",
            GROUP_NO="NO8",
            NOM_CMP="DZ",
            VALE_CALC=-0.067528800000000,
        ),
        _F(
            INST=6.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V1",
            VALE_CALC=3.4999999999963e05,
        ),
        _F(
            INST=20.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V1",
            VALE_CALC=4.4891551712693e05,
        ),
        _F(
            INST=34.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V1",
            VALE_CALC=4.4891551712693e05,
        ),
        _F(
            INST=46.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V1",
            VALE_CALC=4.4891551712693e05,
        ),
        _F(
            INST=60.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V1",
            VALE_CALC=4.4891551712693e05,
        ),
        _F(
            INST=6.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V2",
            VALE_CALC=1.0,
            CRITERE="ABSOLU",
        ),
        _F(
            INST=20.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V2",
            VALE_CALC=1.0,
        ),
        _F(
            INST=34.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V2",
            VALE_CALC=0.0e00,
            CRITERE="ABSOLU",
        ),
        _F(
            INST=46.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V2",
            VALE_CALC=0.0e00,
            CRITERE="ABSOLU",
        ),
        _F(
            INST=60.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V2",
            VALE_CALC=0.0e00,
            CRITERE="ABSOLU",
        ),
        _F(
            INST=6.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V3",
            VALE_CALC=3.6876480360694e05,
        ),
        _F(
            INST=20.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V3",
            VALE_CALC=6.4523156897154e05,
        ),
        _F(
            INST=34.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V3",
            VALE_CALC=6.4523156897154e05,
        ),
        _F(
            INST=46.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V3",
            VALE_CALC=6.4523156897154e05,
        ),
        _F(
            INST=60.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V3",
            VALE_CALC=6.4523156897154e05,
        ),
        _F(
            INST=6.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V4",
            VALE_CALC=0.0e00,
            CRITERE="ABSOLU",
        ),
        _F(
            INST=20.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V4",
            VALE_CALC=0.0e00,
            CRITERE="ABSOLU",
        ),
        _F(
            INST=34.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V4",
            VALE_CALC=0.0e00,
            CRITERE="ABSOLU",
        ),
        _F(
            INST=46.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V4",
            VALE_CALC=0.0e00,
            CRITERE="ABSOLU",
        ),
        _F(
            INST=60.0,
            RESULTAT=U1,
            NOM_CHAM="VARI_NOEU",
            GROUP_NO="NO8",
            NOM_CMP="V4",
            VALE_CALC=0.0e00,
            CRITERE="ABSOLU",
        ),
    )
)

FIN()
