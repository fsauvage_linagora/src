# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2023 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# MODELISATION HEXA27

DEBUT(CODE=_F(NIV_PUB_WEB="INTERNET"), ERREUR=_F(ALARME="EXCEPTION"), DEBUG=_F(SDVERI="OUI"))

MA = LIRE_MAILLAGE(FORMAT="MED", PARTITIONNEUR="PTSCOTCH")

MOTHER = AFFE_MODELE(
    MAILLAGE=MA,
    AFFE=_F(TOUT="OUI", MODELISATION="3D_HHO", FORMULATION="QUADRATIQUE", PHENOMENE="THERMIQUE"),
)
INOX = DEFI_MATERIAU(THER=_F(LAMBDA=19.97e-3, RHO_CP=4.89488e-3))

CHMAT = AFFE_MATERIAU(MAILLAGE=MA, AFFE=_F(TOUT="OUI", MATER=INOX))

PAROI = DEFI_CONSTANTE(VALE=40000.0e-6)


TP_FLUID = DEFI_FONCTION(
    NOM_PARA="INST", VALE=(0.0, 289.0, 12.0, 20.0), PROL_DROITE="CONSTANT", PROL_GAUCHE="CONSTANT"
)

CHAR_TH = AFFE_CHAR_THER_F(
    MODELE=MOTHER, ECHANGE=_F(GROUP_MA="ECHANGE", COEF_H=PAROI, TEMP_EXT=TP_FLUID)
)

LISTTH = DEFI_LIST_REEL(
    DEBUT=0.0,
    INTERVALLE=(
        _F(JUSQU_A=12.0, NOMBRE=12),
        _F(JUSQU_A=20.0, NOMBRE=2),
        _F(JUSQU_A=100.0, NOMBRE=4),
        _F(JUSQU_A=200.0, NOMBRE=2),
        _F(JUSQU_A=400.0, NOMBRE=2),
        _F(JUSQU_A=2000.0, NOMBRE=8),
    ),
)

TEMPE = THER_LINEAIRE(
    MODELE=MOTHER,
    CHAM_MATER=CHMAT,
    EXCIT=_F(CHARGE=CHAR_TH),
    INCREMENT=_F(LIST_INST=LISTTH, INST_INIT=0.0),
    ETAT_INIT=_F(VALE=289.0),
    ARCHIVAGE=_F(
        INST=(12.0, 100.0, 600.0, 2000.0), CRITERE="ABSOLU", PRECISION=1e-8, CHAM_EXCLU=()
    ),
    INFO=2,
)


TEMP_1 = CREA_CHAMP(
    TYPE_CHAM="NOEU_TEMP_R", OPERATION="EXTR", RESULTAT=TEMPE, NOM_CHAM="HHO_TEMP", INST=12.0
)

TEMP_2 = CREA_CHAMP(
    TYPE_CHAM="NOEU_TEMP_R", OPERATION="EXTR", RESULTAT=TEMPE, NOM_CHAM="HHO_TEMP", INST=100.0
)

TEMP_3 = CREA_CHAMP(
    TYPE_CHAM="NOEU_TEMP_R", OPERATION="EXTR", RESULTAT=TEMPE, NOM_CHAM="HHO_TEMP", INST=600.0
)

TEMP_4 = CREA_CHAMP(
    TYPE_CHAM="NOEU_TEMP_R", OPERATION="EXTR", RESULTAT=TEMPE, NOM_CHAM="HHO_TEMP", INST=2000.0
)

TEST_RESU(
    CHAM_NO=(
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M1",
            PRECISION=0.02,
            CHAM_GD=TEMP_1,
            VALE_CALC=288.63566805289406,
            VALE_REFE=288.63999999999999,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M1",
            CHAM_GD=TEMP_2,
            PRECISION=0.05,
            VALE_CALC=203.30135568352063,
            VALE_REFE=202.75999999999999,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M1",
            CHAM_GD=TEMP_3,
            PRECISION=0.02,
            VALE_CALC=93.2954919530848,
            VALE_REFE=93.027000000000001,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M1",
            CHAM_GD=TEMP_4,
            PRECISION=0.02,
            VALE_CALC=29.45240127039312,
            VALE_REFE=29.419,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M2",
            CHAM_GD=TEMP_1,
            VALE_CALC=288.99998900867934,
            VALE_REFE=289.0,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M2",
            PRECISION=0.02,
            CHAM_GD=TEMP_2,
            VALE_CALC=275.0666791731988,
            VALE_REFE=275.04000000000002,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M2",
            CHAM_GD=TEMP_3,
            PRECISION=0.02,
            VALE_CALC=143.02167915778082,
            VALE_REFE=143.0,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M2",
            PRECISION=0.02,
            CHAM_GD=TEMP_4,
            VALE_CALC=35.85846813404976,
            VALE_REFE=35.857999999999997,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M1P",
            PRECISION=0.02,
            CHAM_GD=TEMP_1,
            VALE_CALC=288.6356680528954,
            VALE_REFE=288.63999999999999,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M1P",
            CHAM_GD=TEMP_2,
            PRECISION=0.04,
            VALE_CALC=203.3013556835217,
            VALE_REFE=202.75999999999999,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M1P",
            CHAM_GD=TEMP_3,
            PRECISION=0.02,
            VALE_CALC=93.2954919530851,
            VALE_REFE=93.027000000000001,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M1P",
            CHAM_GD=TEMP_4,
            PRECISION=0.02,
            VALE_CALC=29.45240127039486,
            VALE_REFE=29.419,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M2P",
            CHAM_GD=TEMP_1,
            PRECISION=0.03,
            VALE_CALC=288.9999890086799,
            VALE_REFE=289.0,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M2P",
            PRECISION=0.02,
            CHAM_GD=TEMP_2,
            VALE_CALC=275.0666791731994,
            VALE_REFE=275.04000000000002,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M2P",
            CHAM_GD=TEMP_3,
            PRECISION=0.02,
            VALE_CALC=143.02167915777835,
            VALE_REFE=143.0,
        ),
        _F(
            REFERENCE="AUTRE_ASTER",
            NOM_CMP="TEMP",
            GROUP_NO="M2P",
            PRECISION=0.02,
            CHAM_GD=TEMP_4,
            VALE_CALC=35.85846813404917,
            VALE_REFE=35.857999999999997,
        ),
    )
)

FIN()

#
